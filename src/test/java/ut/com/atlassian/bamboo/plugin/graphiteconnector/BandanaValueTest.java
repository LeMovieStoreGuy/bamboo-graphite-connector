package ut.com.atlassian.bamboo.plugin.graphiteconnector;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.plugin.graphiteconnector.BandanaValue;
import com.atlassian.bandana.BandanaManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created with IntelliJ IDEA.
 * User: dkovacs
 * Date: 24/06/13
 * Time: 1:20 PM
 */

@RunWith(MockitoJUnitRunner.class)
public class BandanaValueTest {

    @Mock
    private BandanaManager bandanaManager;


    @Test
    public void TestGetStringValue() {
        String expectedValue = "test value";
        String valueKey = "MyKey";

        when( bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, valueKey) ).thenReturn(expectedValue);

        BandanaValue<String> value = new BandanaValue<String>( bandanaManager, valueKey, "my default value" );

        String returnedValue = value.get();

        assertEquals(String.format("Returned value \"%s\" does not match expected: \"%s\".", expectedValue, returnedValue), expectedValue, returnedValue);
    }

    @Test
    public void TestSetStringValue() {
        String expectedValue = "test value";
        String valueKey = "MyKey";

        BandanaValue<String> value = new BandanaValue<String>( bandanaManager, valueKey, "my default value" );
        value.set( expectedValue );

        verify(bandanaManager).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, valueKey, expectedValue);
    }


    @Test
    public void TestGetIntegerValue() {
        int expectedValue = 1982;
        String valueKey = "MyKey";

        when( bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, valueKey) ).thenReturn(expectedValue);

        BandanaValue<Integer> value = new BandanaValue<Integer>( bandanaManager, valueKey, 9999 );

        int returnedValue = value.get();

        assertEquals(String.format("Returned value \"%d\" does not match expected: \"%d\".", expectedValue, returnedValue), expectedValue, returnedValue );
    }

    @Test
    public void TestSetIntegerValue() {
        int expectedValue = 1982;
        String valueKey = "MyKey";

        BandanaValue<Integer> value = new BandanaValue<Integer>( bandanaManager, valueKey, 9999 );

        value.set( expectedValue );

        verify(bandanaManager).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, valueKey, expectedValue);
    }

    @Test(expected = RuntimeException.class)
    public void TestIfFaultyBandanaValueGetsRemoved() {
        int expectedValue = 1982;
        String valueKey = "MyKey";

        when( bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, valueKey) ).thenReturn("faulty string value");

        BandanaValue<Integer> value = new BandanaValue<Integer>( bandanaManager, valueKey, expectedValue );

        int returnedValue = value.get();

    }


}
