package com.atlassian.bamboo.plugin.graphiteconnector;

/**
 * GraphiteConnectorSender Interface provides methods to send data to graphite.
 */
public interface GraphiteConnectorSender {

    /**
     * Write a measurement with automatic timestamp.
     *
     * Automatic timestamp means the server time at the moment of calling.
     *
     * @param path Graphite path of the measurement
     * @param label Label of the measurement
     * @param value Value of the measurement
     */
    void write( String path, String label, String value);

    /**
     * Write a measurement with specified timestamp
     * @param path Graphite path of the measurement
     * @param label Label of the measurement
     * @param value Value of the measurement
     * @param timestamp Timestamp of the measurement in seconds.
     */
    void write( String path, String label, String value, long timestamp );

}
